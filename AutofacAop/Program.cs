﻿using System;
using System.Threading;

namespace AutofacAop
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var app = new AutofacAopApp();
            app.Start();

            Console.WriteLine("press any key");
            Console.ReadKey();
        }
    }
}
