﻿using System;

namespace AutofacAop
{
    public interface IDatabase
    {
        string Get(string key);
    }

    public class Database : IDatabase
    {
        [CacheResult(Duration = 600)]
        [ConsoleLogging]
        public string Get(string key)
        {
            return Guid.NewGuid().ToString("N");
        }
    }
}