﻿using System;
using System.Diagnostics;
using Castle.DynamicProxy;

namespace AutofacAop
{
    public class LoggingInterceptor : IInterceptor
    {
        public ConsoleLoggingAttribute GetCacheOptionAttribute(IInvocation invocation)
        {
            return Attribute.GetCustomAttribute(
                invocation.MethodInvocationTarget,
                typeof(ConsoleLoggingAttribute)
            )
            as ConsoleLoggingAttribute;
        }

        public void Intercept(IInvocation invocation)
        {
            // Recherche de l'attribut CacheResultAttribute sur la méthode invoquée
            var writerAttr = GetCacheOptionAttribute(invocation);

            // On n'applique pas le cache si l'attribut n'est pas présent
            if (writerAttr == null)
            {
                invocation.Proceed();
                return;
            }
            
            var watch = Stopwatch.StartNew();
            invocation.Proceed();
            watch.Stop();

            var fullMethodName = String.Format("{0}.{1}", invocation.TargetType.FullName, invocation.Method.Name);

            Console.WriteLine("{0} returned {1} in {2} sec",
                fullMethodName,
                invocation.ReturnValue,
                watch.Elapsed.TotalSeconds
            );
        }
    }
}
