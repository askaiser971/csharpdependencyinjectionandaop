﻿using Autofac;
using Autofac.Extras.DynamicProxy2;

namespace AutofacAop
{
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<CacheAspectInterceptor>().SingleInstance();
            builder.RegisterType<LoggingInterceptor>().SingleInstance();

            builder.RegisterType<Database>()
                .As<IDatabase>()
                .EnableInterfaceInterceptors()
                .InterceptedBy(typeof(LoggingInterceptor))
                .InterceptedBy(typeof(CacheAspectInterceptor));

            builder.RegisterType<Worker>().As<IWorker>();
        }
    }
}
