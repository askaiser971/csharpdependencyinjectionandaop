﻿using System;
using System.Linq;
using System.Runtime.Caching;
using Castle.DynamicProxy;

namespace AutofacAop
{
    public class CacheAspectInterceptor : IInterceptor
    {
        public CacheResultAttribute GetCacheOptionAttribute(IInvocation invocation)
        {
            return Attribute.GetCustomAttribute(
                invocation.MethodInvocationTarget,
                typeof(CacheResultAttribute)
            )
            as CacheResultAttribute;
        }

        public string GetInvocationSignature(IInvocation invocation)
        {
            return String.Format("{0}-{1}-{2}",
                invocation.TargetType.FullName,
                invocation.Method.Name,
                String.Join("-", invocation.Arguments.Select(a => (a ?? "").ToString()).ToArray())
            );
        }

        public void Intercept(IInvocation invocation)
        {
            // Recherche de l'attribut CacheResultAttribute sur la méthode invoquée
            var optionsAttr = GetCacheOptionAttribute(invocation);

            // On n'applique pas le cache si l'attribut n'est pas présent
            if (optionsAttr == null)
            {
                invocation.Proceed();
                return;
            }

            // Calcul de la clef unique de la méthode invoquée basée sur le nom de la classe, le nom de la méthode et les args de la méthode
            string key = GetInvocationSignature(invocation);

            ObjectCache cache = MemoryCache.Default;
            var result = cache[key];

            // Pas de valeur en cache pour cet appel
            if (result == null)
            {
                invocation.Proceed();
                result = invocation.ReturnValue;

                var policy = new CacheItemPolicy
                {
                    AbsoluteExpiration = DateTime.Now.AddMilliseconds(optionsAttr.Duration)
                };

                if (result != null)
                    cache.Set(key, result, policy);
            }
            else
            {
                invocation.ReturnValue = result;
            }
        }
    }
}
