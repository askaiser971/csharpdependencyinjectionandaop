﻿using System;
using System.Threading;

namespace AutofacAop
{
    public interface IWorker
    {
        void DoWork();
    }

    public class Worker : IWorker
    {
        private readonly IDatabase _db;

        public Worker(IDatabase db)
        {
            _db = db;
        }

        public void DoWork()
        {
            _db.Get("a");
            _db.Get("b");

            Thread.Sleep(500);

            _db.Get("a");
        }
    }
}
