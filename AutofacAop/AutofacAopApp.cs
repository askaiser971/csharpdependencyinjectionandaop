﻿using Autofac;

namespace AutofacAop
{
    public class AutofacAopApp
    {
        private readonly IContainer _container;

        public AutofacAopApp()
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule<AutofacModule>();
            _container = builder.Build();
        }

        public void Start()
        {
            var worker = _container.Resolve<IWorker>();
            worker.DoWork();
        }
    }
}