﻿using System;

namespace AutofacAop
{
    /// <summary>
    /// Attribut permettant de déclencher la mise en cache d'une méthode de classe
    /// lorsque cette classe est interceptée par <see cref="AutofacAop.CacheAspectInterceptor"/>
    /// </summary>
    public class CacheResultAttribute : Attribute
    {
        /// <summary>
        /// Durée par défaut du cache en millisecondes
        /// </summary>
        public const int DefaultDuration = 1000;

        /// <summary>
        /// Durée du cache en millisecondes
        /// </summary>
        public int Duration { get; set; }
    }
}
