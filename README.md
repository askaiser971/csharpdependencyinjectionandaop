Projet C# basique illustrant l'utilisation d'une librairie d'injection de dépendance (Autofac) et la programmation orientée aspect (Castle DynamicProxy, via un wrapper Autofac).

**Exemple d'enregistrement des dépendances via un module Autofac:**

```
#!csharp

public class AutofacModule : Module
{
    protected override void Load(ContainerBuilder builder)
    {
        builder.RegisterType<CacheAspectInterceptor>().SingleInstance();
        builder.RegisterType<LoggingInterceptor>().SingleInstance();

        builder.RegisterType<Database>()
            .As<IDatabase>()
            .EnableInterfaceInterceptors()
            .InterceptedBy(typeof(LoggingInterceptor))
            .InterceptedBy(typeof(CacheAspectInterceptor));

        builder.RegisterType<Worker>().As<IWorker>();
    }
}
```

On peut récupérer n'importe quelle instance de dépendance avec le container Autofac (utilisé principalement au démarrage de l'application):

```
#!csharp

var worker = _container.Resolve<IWorker>();
worker.DoWork();
```